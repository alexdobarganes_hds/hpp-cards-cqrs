using AutoFixture;
using Convey.CQRS.Commands;
using HDS.PPC.Cards.Application.Commands;
using HDS.PPC.Cards.Application.Commands.Handlers;
using HDS.PPC.Cards.Application.Repositories;
using HDS.PPC.Cards.Domain.Aggregates;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace HDS.PPC.Cards.Application.Tests.Commands
{

    public class CreateCardHandlerTests
    {
        private readonly Fixture _fixture;
        private readonly Mock<IRepository<Card>> _repositoryMock;
        private readonly ICommandHandler<CreateCard> _sut;

        private Task Act(CreateCard command) => _sut.HandleAsync(command);

        public CreateCardHandlerTests()
        {
            _fixture = new Fixture();
            _repositoryMock = new Mock<IRepository<Card>>();
            _repositoryMock.Setup(r => r.UnitOfWork.SaveEntitiesAsync(It.IsAny<CancellationToken>()));
            _sut = new CreateCardHandler(_repositoryMock.Object);
        }

        //[Fact]
        //public async Task ShouldCreateNewResource()
        //{
        //    //Given
        //    var command = new CreateCard(_fixture.Create<string>(), _fixture.Create<string>(), _fixture.Create<int>(), true, _fixture.Create<DateTimeOffset>());
        //    Card resource = null;

        //    _repositoryMock.Setup(r => r.AddAsync(It.IsAny<Card>())).Callback<Card>(res =>
        //    {
        //        resource = res;
        //    });

        //    //When
        //    await Act(command);

        //    //Then
        //    _repositoryMock.Verify(x => x.AddAsync(It.IsAny<Card>()), Times.AtLeastOnce);
        //    _repositoryMock.Verify(x => x.UnitOfWork.SaveEntitiesAsync(default), Times.Once);
        //    Assert.Equal(command.CardId, resource.Id);
        //}
    }
}