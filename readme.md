    List<Card> getCardsByXref(String xref);

    Card getCardByID(String cardID);

    Card updateCards(String cardId, Card card);

    List<Card> createCards(String userID, List<Card> cards) throws IOException;

    List<Card> getCardsByAccountNum(String accountNum);

    List<Card> findNewCards();

    List<Card> findExpiringCards();

    void updateHistory(String notificationTaskId, JsonNode cardUpdateHistoryList);