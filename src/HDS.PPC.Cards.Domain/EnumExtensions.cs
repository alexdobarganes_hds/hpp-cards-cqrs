﻿using System;
using System.ComponentModel;
using System.Linq;

namespace HDS.PPC.Cards.Domain
{
    public static class EnumExtensions
    {
        public static string Description(this Enum enumValue)
        {
            var descriptionAttribute = enumValue.GetType()
                .GetField(enumValue.ToString())
                .GetCustomAttributes(false)
                .SingleOrDefault(attr => attr.GetType() == typeof(DescriptionAttribute)) as DescriptionAttribute;

            // return description
            return descriptionAttribute?.Description ?? "";
        }
    }
}
