using HDS.PPC.Cards.Domain.Aggregates;
using System;
using System.Threading.Tasks;

namespace HDS.PPC.Cards.Domain.Repositories
{
    public interface IResourceRepository
    {
        Task<Card> GetAsync(Guid id);
        Task AddAsync(Card Resource);
        Task UpdateAsync(Card Resource);
        Task DeleteAsync(Card id);
    }
}