﻿namespace HDS.PPC.Cards.Domain
{
    public abstract class Entity<TId>
    {
        public TId Id { get; set; }
    }
}
