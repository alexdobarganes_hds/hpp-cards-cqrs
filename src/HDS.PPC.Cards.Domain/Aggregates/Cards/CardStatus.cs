﻿namespace HDS.PPC.Cards.Domain.Aggregates
{
    public enum CardStatus {
        Requested,
        Created,
        Active,
        Disabled
    }
}