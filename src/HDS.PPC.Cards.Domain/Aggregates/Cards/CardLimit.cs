﻿using System;

namespace HDS.PPC.Cards.Domain.Aggregates
{
    public record CardLimit
    {
        public int? Amount { get; protected set; }

        public LimitFrequency Frequency { get; protected set; }

        public CardLimit(LimitFrequency frequency, int? amount = null)
        {
            Frequency = frequency;
            if (Frequency == LimitFrequency.None)
                return;
            if (amount.HasValue)
            {
                if(amount.Value < 0)
                    throw new ArgumentOutOfRangeException(nameof(amount), "Must be greater than 0");
                Amount = amount.Value;
            }
        }

        public static CardLimit NoLimit() => new CardLimit(LimitFrequency.None);

        public static CardLimit Weekly(int amount) => new CardLimit(LimitFrequency.Week, amount);
        
        public static CardLimit Monthly(int amount) => new CardLimit(LimitFrequency.Month, amount);

        public static CardLimit Create(int cardLimitAmount, string cardLimitFrequency)
        {
            if(cardLimitFrequency.ToLower().StartsWith("month"))
                return Monthly(cardLimitAmount);
            if(cardLimitFrequency.ToLower().StartsWith("week"))
                return Weekly(cardLimitAmount);
            
            return NoLimit();
        }
    }
}