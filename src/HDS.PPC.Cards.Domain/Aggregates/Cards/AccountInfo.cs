﻿namespace HDS.PPC.Cards.Domain.Aggregates
{
    public record AccountInfo
    {
        public string ServiceAccountNumber { get; protected set; }

        public AccountInfo(string serviceAccountNumber)
        {
            ServiceAccountNumber = serviceAccountNumber;
        }

        public static AccountInfo Create(string serviceAccountNumber) 
        {
            return new AccountInfo(serviceAccountNumber);
        }
    }
}