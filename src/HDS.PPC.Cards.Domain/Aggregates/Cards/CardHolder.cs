﻿namespace HDS.PPC.Cards.Domain.Aggregates
{
    public record CardHolder
    {
        public string EReceiptEmail { get; protected set; }
        public string CardHolderFirstName { get; protected set; }
        public string CardHolderLastName { get; protected set; }

        public CardHolder(string cardHolderFirstName, string cardHolderLastName, string eReceiptEmail)
        {
            CardHolderFirstName = cardHolderFirstName;
            CardHolderLastName = cardHolderLastName;
            EReceiptEmail = eReceiptEmail;
        }

        public static CardHolder Create(string cardHolderFirstName, string cardHolderLastName, string eReceiptEmail)
        {
            return new CardHolder(cardHolderFirstName, cardHolderLastName, eReceiptEmail);
        }
    }
}