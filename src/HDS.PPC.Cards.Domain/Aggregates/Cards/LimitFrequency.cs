﻿namespace HDS.PPC.Cards.Domain.Aggregates
{
    public enum LimitFrequency
    {
        None,
        Week,
        Month
    }
}