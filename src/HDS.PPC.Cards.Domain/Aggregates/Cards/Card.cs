using HDS.PPC.Cards.Domain.Events;
using System;
using System.Collections.Generic;

namespace HDS.PPC.Cards.Domain.Aggregates
{
    public class Card : AggregateRoot
    {
        public string CardNickName { get; protected set; }
        public string Zip { get; set; }
        public bool IsHouseAccount { get; set; }
        public DateTimeOffset ExpirationDate { get; protected set; }
        public AccountInfo AccountInfo { get; }
        public CardHolder CardHolder { get; protected set; }
        public CardXRef CardXRef { get; }
        public CardLimit CardLimit { get; set; }
        public CardStatus Status { get; protected set; }
        public List<CardActivity> CardActivity { get; protected set; }
        public DateTimeOffset LastActivity { get; protected set; }

        protected Card() { }

        private Card(Guid id, string cardNickName, string zip, bool isHouseAccount,
            DateTimeOffset expirationDate,
            AccountInfo accountInfo,
            CardHolder cardHolder,
            CardXRef cardXRef,
            CardLimit cardLimit,
            DateTimeOffset lastActivity)
        {
            Id = id;
            CardNickName = cardNickName;
            Zip = zip;
            IsHouseAccount = isHouseAccount;
            ExpirationDate = expirationDate;
            AccountInfo = accountInfo;
            CardHolder = cardHolder;
            CardXRef = cardXRef;
            CardLimit = cardLimit;
            Status = CardStatus.Created;
            LastActivity = lastActivity;

            AddEvent(new CardStateChanged(this));
        }

        public static Card Create(Guid id, string cardNickName, string zip, bool isHouseAccount,
            DateTimeOffset expirationDate,
            AccountInfo accountInfo,
            CardHolder cardHolder,
            CardXRef cardXRef,
            CardLimit cardLimit,
            DateTimeOffset created)
        {
            return new Card(id, cardNickName, zip, isHouseAccount, expirationDate, accountInfo, cardHolder, cardXRef, cardLimit, created); ;
        }

        public void Disable(DateTimeOffset disabledOn)
        {
            GuardInactiveCard();

            Status = CardStatus.Disabled;
            LastActivity = disabledOn;
            CardActivity.Add(new CardActivity());

            AddEvent(new CardStateChanged(this));
        }

        public void Request(DateTimeOffset requestedOn)
        {
            GuardInactiveCard();

            if (Status == CardStatus.Requested)
                throw new InvalidOperationException("Card already requested");

            Status = CardStatus.Requested;
            LastActivity = requestedOn;
            CardActivity.Add(new CardActivity());

            AddEvent(new CardStateChanged(this));
        }

        private void GuardInactiveCard() {
            if (Status == CardStatus.Disabled)
                throw new InvalidOperationException("Card already disabled");
        }

        private void GuardActiveCard()
        {
            if (Status != CardStatus.Active)
                throw new InvalidOperationException("Card must be active");
        }
    }
}