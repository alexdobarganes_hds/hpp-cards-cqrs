﻿using System;

namespace HDS.PPC.Cards.Domain.Aggregates
{
    public record CardXRef
    {
        public string Value { get; protected set; }

        public CardXRef(string xRef)
        {
            if (string.IsNullOrWhiteSpace(xRef))
                throw new ArgumentException($"'{nameof(xRef)}' cannot be null or whitespace.", nameof(xRef));

            Value = xRef;
        }
        public static CardXRef Create(string xRef)
        {
            return new CardXRef(xRef);
        }
    }
}