using HDS.PPC.Cards.Domain.Events;
using System.Collections.Generic;

namespace HDS.PPC.Cards.Domain.Aggregates
{
    public interface IAggregateRoot
    {
        IEnumerable<IDomainEvent> Events { get; }
    }
}
