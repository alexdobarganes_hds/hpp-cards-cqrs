using System;

namespace HDS.PPC.Cards.Domain.Exceptions
{
    public class CannotChangeCardStateException : DomainException
    {
        public override string Code { get; } = "cannot_change_card_state";

        public CannotChangeCardStateException(Guid id) :
            base($"Cannot change state for Card with id: '{id}'")
        { }
    }
}