﻿using System.Threading.Tasks;

namespace HDS.PPC.Cards.Domain.Events
{
    public interface IDomainEventHandler<in T> where T : class, IDomainEvent
    {
        Task HandleAsync(T @event);
    }
}
