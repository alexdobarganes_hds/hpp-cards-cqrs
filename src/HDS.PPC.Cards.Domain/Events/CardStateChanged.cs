﻿using HDS.PPC.Cards.Domain.Aggregates;

namespace HDS.PPC.Cards.Domain.Events
{
    public record CardStateChanged : IDomainEvent
    {
        public Card Card { get; }

        public CardStateChanged(Card card)
        {
            Card = card;
        }
    }
}
