﻿using Convey.CQRS.Commands;
using System;

namespace HDS.PPC.Cards.Application.Commands
{
    //Changes the state of the card to Created once it has been printed
    public record RequestCard : ICommand
    {
        public Guid CardId { get; set; }

        public DateTimeOffset RequestedOn { get; set; }

        public RequestCard(Guid cardId, DateTimeOffset requestedOn)
        {
            CardId = cardId;
            RequestedOn = requestedOn;
        }
    }
}