﻿using Convey.CQRS.Commands;
using HDS.PPC.Cards.Application.Repositories;
using HDS.PPC.Cards.Domain.Aggregates;
using System;
using System.Threading.Tasks;

namespace HDS.PPC.Cards.Application.Commands.Handlers
{
    public class DisableCardHandler : ICommandHandler<DisableCard>
    {
        private readonly IRepository<Card> _repository;

        public DisableCardHandler(IRepository<Card> repository)
        {
            _repository = repository;
        }
        public async Task HandleAsync(DisableCard command)
        {
            var card = await _repository.GetAsync(command.CardId);
            if (card == null)
                throw new ApplicationException($"Card with id {command.CardId} is not found!");

            card.Disable(command.DisabledOn);

            await _repository.UpdateAsync(card);
            await _repository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}