﻿using Convey.CQRS.Commands;
using HDS.PPC.Cards.Application.Repositories;
using HDS.PPC.Cards.Domain.Aggregates;
using System;
using System.Threading.Tasks;

namespace HDS.PPC.Cards.Application.Commands.Handlers
{
    public class CreateCardHandler : ICommandHandler<CreateCard>
    {
        private readonly IRepository<Card> _repository;

        public CreateCardHandler(IRepository<Card> repository)
        {
            _repository = repository;
        }
        public async Task HandleAsync(CreateCard command)
        {
            var resource = Card.Create(command.CardId, command.CardNickName, command.CardZip, 
                command.IsHouseAccount,
                command.ExpirationDate,
                AccountInfo.Create(command.ServiceAccountNumber),
                CardHolder.Create(command.CardHolderFirstName, command.CardHolderLastName, command.EReceiptEmail),
                CardXRef.Create(command.XRef),
                CardLimit.Create(command.CardLimitAmount, command.CardLimitFrequency),
                DateTimeOffset.Now);
            
            await _repository.AddAsync(resource);
            await _repository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}