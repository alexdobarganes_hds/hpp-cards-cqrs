﻿using Convey.CQRS.Commands;
using HDS.PPC.Cards.Application.Repositories;
using HDS.PPC.Cards.Domain.Aggregates;
using System;
using System.Threading.Tasks;

namespace HDS.PPC.Cards.Application.Commands.Handlers
{
    public class RequestCardHandler : ICommandHandler<RequestCard>
    {
        private readonly IRepository<Card> _repository;

        public RequestCardHandler(IRepository<Card> repository)
        {
            _repository = repository;
        }
        public async Task HandleAsync(RequestCard command)
        {
            var card = await _repository.GetAsync(command.CardId);
            if (card == null)
                throw new ApplicationException($"Card with id {command.CardId} is not found!");

            card.Request(command.RequestedOn);

            await _repository.UpdateAsync(card);
            await _repository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}