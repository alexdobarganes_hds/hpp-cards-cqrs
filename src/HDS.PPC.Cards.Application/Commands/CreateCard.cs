using Convey.CQRS.Commands;
using Convey.WebApi.CQRS;
using System;

namespace HDS.PPC.Cards.Application.Commands
{

    [PublicContract]
    public record CreateCard : ICommand
    {
        public Guid CardId { get; set; }
        public string CardNickName { get; set; }
        public string CardZip { get; set; }
        public int BuyerId { get; set; }
        public bool IsHouseAccount { get; set; }
        public DateTimeOffset ExpirationDate { get; set; }
        public string XRef { get; set; }
        public int CardLimitAmount { get; set; }        
        public string CardLimitFrequency { get; set; }
        public string EReceiptEmail { get; set; }
        public string CardHolderFirstName { get; set; }
        public string CardHolderLastName { get; set; }
        public string ServiceAccountNumber { get; set; }

        public CreateCard(Guid cardId)
        {
            CardId = cardId == Guid.Empty ? Guid.NewGuid() : cardId;
        }
    }
}