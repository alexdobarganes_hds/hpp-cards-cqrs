﻿using Convey.CQRS.Commands;
using System;

namespace HDS.PPC.Cards.Application.Commands
{
    public record DisableCard : ICommand
    {
        public Guid CardId { get; set; }

        public DateTimeOffset DisabledOn { get; set; }

        public DisableCard(Guid cardId, DateTimeOffset disabledOn)
        {
            CardId = cardId;
            DisabledOn = disabledOn;
        }
    }
}