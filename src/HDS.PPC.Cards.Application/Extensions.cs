using Convey;
using Convey.CQRS.Commands;
using Convey.CQRS.Events;
using Convey.CQRS.Queries;
using HDS.PPC.Cards.Application.Mapper;
using Microsoft.Extensions.DependencyInjection;

namespace HDS.PPC.Cards.Application
{
    public static class Extensions
    {
        public static IConveyBuilder AddApplication(this IConveyBuilder builder)
        {
            builder.Services.AddAutoMapper(new[] { typeof(CardMapper).Assembly }, ServiceLifetime.Scoped);
            return builder.AddQueryHandlers()
                          .AddCommandHandlers()
                          .AddEventHandlers()
                          .AddInMemoryQueryDispatcher()
                          .AddInMemoryCommandDispatcher()
                          .AddInMemoryEventDispatcher();
        }
    }
}