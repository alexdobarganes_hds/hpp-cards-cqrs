﻿using System;

namespace HDS.PPC.Cards.Application.DTO
{
    public abstract class Dto
    {
        public Guid Id { get; set; }
    }
}
