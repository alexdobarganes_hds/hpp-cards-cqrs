﻿using HDS.PPC.Cards.Domain.Aggregates;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HDS.PPC.Cards.Application.Repositories
{
    public interface IReadRepository<TAggregate, TDto, in TKey>
        where TAggregate : AggregateRoot
        where TDto : class
    {
        Task<TDto> GetAsync(TKey id);
        Task<IEnumerable<TDto>> FindAsync(Expression<Func<TAggregate, bool>> findPredicate);
    }
}
