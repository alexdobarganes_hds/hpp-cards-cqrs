﻿using HDS.PPC.Cards.Domain.Events;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HDS.PPC.Cards.Application.Services
{
    public interface IEventProcessor
    {
        Task ProcessAsync(IEnumerable<IDomainEvent> events);
    }
}