using Convey.CQRS.Events;
using HDS.PPC.Cards.Domain.Events;
using System.Collections.Generic;

namespace HDS.PPC.Cards.Application.Services
{
    public interface IEventMapper
    {
        IEvent Map(IDomainEvent @event);
        IEnumerable<IEvent> MapAll(IEnumerable<IDomainEvent> events);
    }
}