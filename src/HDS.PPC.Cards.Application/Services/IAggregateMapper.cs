﻿using System.Collections.Generic;

namespace HDS.PPC.Cards.Application.Services
{
    public interface IAggregateMapper<in TAggregate, out TDto>
    {
        TDto Map(TAggregate source);

        IEnumerable<TDto> MapAll(IEnumerable<TAggregate> source);
    }
}
