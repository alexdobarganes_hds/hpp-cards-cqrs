using Convey.CQRS.Events;
using HDS.PPC.Cards.Application.Events;
using HDS.PPC.Cards.Application.Services;
using HDS.PPC.Cards.Domain.Aggregates;
using HDS.PPC.Cards.Domain.Events;
using System.Collections.Generic;
using System.Linq;

namespace HDS.PPC.Cards.Infrastructure.Services
{
    public class EventMapper : IEventMapper
    {
        public IEnumerable<IEvent> MapAll(IEnumerable<IDomainEvent> events)
            => events.Select(Map);

        public IEvent Map(IDomainEvent @event)
        {
            switch (@event)
            {
                case CardStateChanged e:
                    switch (e.Card.Status)
                    {
                        case CardStatus.Created:
                            return new CardCreated(e.Card.Id, e.Card.ExpirationDate, e.Card.CardHolder.CardHolderFirstName);
                        case CardStatus.Requested:
                            return new CardRequested(e.Card.Id);
                        case CardStatus.Disabled:
                            return new CardDisabled(e.Card.Id);
                    }
                    break;
            }

            return null;
        }
    }
}