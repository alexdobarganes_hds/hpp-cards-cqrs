﻿using Convey.CQRS.Queries;
using HDS.PPC.Cards.Application.DTO;
using HDS.PPC.Cards.Application.Repositories;
using HDS.PPC.Cards.Domain.Aggregates;
using System;
using System.Threading.Tasks;

namespace HDS.PPC.Cards.Application.Queries.Handlers
{
    public class GetCardHandler : IQueryHandler<GetCard, CardDto>
    {
        private readonly IReadRepository<Card, CardDto, Guid> _readRepository;

        public GetCardHandler(IReadRepository<Card, CardDto, Guid> readRepository)
        {
            _readRepository = readRepository;
        }

        public async Task<CardDto> HandleAsync(GetCard query)
        {
            var Resource = await _readRepository.GetAsync(query.CardId);

            return Resource;
        }
    }
}
