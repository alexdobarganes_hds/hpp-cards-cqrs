﻿using Convey.CQRS.Queries;
using Convey.WebApi.CQRS;
using HDS.PPC.Cards.Application.DTO;
using System;

namespace HDS.PPC.Cards.Application.Queries
{
    [PublicContract]
    public class GetCard : IQuery<CardDto>
    {
        public Guid CardId { get; set; }

        public GetCard(Guid cardId)
        {
            CardId = cardId;
        }
    }
}
