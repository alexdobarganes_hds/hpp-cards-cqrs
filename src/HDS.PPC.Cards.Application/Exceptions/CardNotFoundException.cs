using System;

namespace HDS.PPC.Cards.Application.Exceptions
{
    public class CardNotFoundException : AppException
    {
        public override string Code { get; } = "card_not_found";
        public Guid CardId { get; }

        public CardNotFoundException(Guid cardId) : base($"Card with id: {cardId} was not found.")
            => CardId = cardId;
    }
}