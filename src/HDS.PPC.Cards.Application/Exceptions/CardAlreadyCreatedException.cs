using System;

namespace HDS.PPC.Cards.Application.Exceptions
{
    public class CardAlreadyCreatedException : AppException
    {
        public override string Code { get; } = "card_already_created";

        public CardAlreadyCreatedException(Guid cardId) : base($"Card with id: {cardId} was already created.")
        {
        }
    }
}