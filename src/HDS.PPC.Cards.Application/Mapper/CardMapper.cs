﻿using AutoMapper;
using HDS.PPC.Cards.Application.DTO;
using HDS.PPC.Cards.Domain.Aggregates;

namespace HDS.PPC.Cards.Application.Mapper
{
    public class CardMapper : Profile
    {
        public CardMapper()
        {
            CreateMap<Card, CardDto>();
                //.ForMember(x => x.ProcessedCards, opt => opt.MapFrom(src => src.HistoryRecords.Where(x => x.Status != NotificationStatus.NotSent)))
                //.ForMember(x => x.ExcludedCards, opt => opt.MapFrom(src => src.HistoryRecords.Where(x => x.Status == NotificationStatus.NotSent)));
            //CreateMap<CardActivity, CardActivityDto>()
            //    .ForMember(x => x.CardId, opt => opt.MapFrom(src => src.CardId))
            //    .ForMember(x => x.Reason, opt => opt.MapFrom(src => src.NotSentReason))
            //    .ForMember(x => x.EventType, opt => opt.MapFrom(src => src.EventType.ToString()));
        }
    }
}
