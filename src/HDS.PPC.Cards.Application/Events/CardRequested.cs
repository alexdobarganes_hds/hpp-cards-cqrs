﻿using System;
using Convey.CQRS.Events;

namespace HDS.PPC.Cards.Application.Events
{
    public record CardRequested : IEvent
    {
        public Guid CardId { get; set; }

        public CardRequested(Guid cardId)
        {
            CardId = cardId;
        }
    }
}
