﻿using System;
using Convey.CQRS.Events;

namespace HDS.PPC.Cards.Application.Events
{
    //TODO initially use only event notification pattern
    public record CardCreated : IEvent
    {
        public Guid CardId { get; set; }

        public DateTimeOffset ExpirationDate { get; set; }

        public string CardHolderFirstName { get; set; }

        public CardCreated(Guid cardId, DateTimeOffset expirationDate, string cardHolderFirstName)
        {
            CardId = cardId;
            ExpirationDate = expirationDate;
            CardHolderFirstName = cardHolderFirstName;
        }
    }
}
