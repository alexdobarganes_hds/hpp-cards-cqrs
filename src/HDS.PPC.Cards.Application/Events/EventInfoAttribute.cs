﻿using System;

namespace HDS.PPC.Cards.Application.Events
{
    public class PubSubInfoAttribute : Attribute
    {
        public string TopicId { get; set; }

        public string Prefix { get; set; }

        public PubSubInfoAttribute(string topicId, string prefix)
        {
            TopicId = topicId;
            Prefix = prefix;
        }
    }
}
