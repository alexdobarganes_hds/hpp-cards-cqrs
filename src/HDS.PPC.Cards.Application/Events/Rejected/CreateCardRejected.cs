﻿using Convey.CQRS.Events;
using System;

namespace HDS.PPC.Cards.Application.Events.Rejected
{
    public class CreateCardRejected : IEvent
    {
        public Guid CardId { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }

        public CreateCardRejected(Guid cardId, string message, string code)
        {
            CardId = cardId;
            Message = message;
            Code = code;
        }
    }
}