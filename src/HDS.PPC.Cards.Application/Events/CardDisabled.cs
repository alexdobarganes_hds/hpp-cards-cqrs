﻿using System;
using Convey.CQRS.Events;

namespace HDS.PPC.Cards.Application.Events
{
    public record CardDisabled : IEvent
    {
        public Guid CardId { get; set; }

        public CardDisabled(Guid cardId)
        {
            CardId = cardId;
        }
    }
}
