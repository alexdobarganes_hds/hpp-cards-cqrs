using Convey;
using Convey.Logging;
using Convey.Types;
using Convey.WebApi;
using Convey.WebApi.CQRS;
using HDS.PPC.Cards.Application;
using HDS.PPC.Cards.Application.Commands;
using HDS.PPC.Cards.Application.DTO;
using HDS.PPC.Cards.Application.Queries;
using HDS.PPC.Cards.Infrastructure;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Formatting.Json;
using System.Threading.Tasks;

namespace HDS.PPC.Cards
{
    public class Program
    {
        public static async Task Main(string[] args)
            => await WebHost.CreateDefaultBuilder(args)
                .ConfigureServices(services => services
                    .AddConvey()
                    .AddWebApi()
                    .AddApplication()
                    .AddInfrastructure()
                    .Build())
                .Configure(app => app
                    .UseApp()
                    .UseRouting()
                    .UsePublicContracts()
                    .UseEndpoints(endpoints => endpoints.MapHealthChecks("/health"))
                    .UseDispatcherEndpoints(endpoints => endpoints
                        .Get("", ctx => ctx.Response.Ok(app.ApplicationServices.GetService<AppOptions>().Name))
                        .Get<GetCard, CardDto>("cards/{cardId}")
                        .Post<CreateCard>("cards",
                            afterDispatch: (cmd, ctx) => ctx.Response.Created($"cards/{cmd.CardId}"))))
                .UseLogging((c, config) => config.WriteTo.Console(new JsonFormatter()))
                .Build()
                .RunAsync();
    }
}