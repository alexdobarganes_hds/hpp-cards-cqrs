﻿using System;

namespace HDS.PPC.Cards.Infrastructure.Exceptions
{
    public interface IExceptionToMessageMapper
    {
        object Map(Exception exception, object message);
    }
}