﻿using HDS.PPC.Cards.Application.DTO;
using HDS.PPC.Cards.Application.Services;
using HDS.PPC.Cards.Domain.Aggregates;
using System.Collections.Generic;
using System.Linq;

namespace HDS.PPC.Cards.Infrastructure.Services
{
    public class ResourceMapper : IAggregateMapper<Card, CardDto>
    {
        public CardDto Map(Card source) => source.AsDto();

        public IEnumerable<CardDto> MapAll(IEnumerable<Card> source) => source.Select(Map);
    }

    public static class ResourceMapperExtensions
    {
        public static CardDto AsDto(this Card resource)
        {
            //Perform custom mapping
            return new CardDto();
        }
    }
}
