﻿using HDS.PPC.Cards.Application.Repositories;
using HDS.PPC.Cards.Application.Services;
using HDS.PPC.Cards.Domain.Aggregates;
using HDS.PPC.Cards.Infrastructure.EntityFramework.Configuration;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace HDS.PPC.Cards.Infrastructure.EntityFramework
{
    public class CardsDbContext : DbContext, IUnitOfWork
    {
        private readonly IEventProcessor _eventProcessor;
        public DbSet<Card> Cards { get; protected set; }
        public IEventProcessor EventProcessor => _eventProcessor;

        public CardsDbContext(DbContextOptions<CardsDbContext> options, IEventProcessor eventProcessor) : base(options)
        {
            _eventProcessor = eventProcessor;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CardConfiguration());
            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                // Replace table names
                entity.SetTableName(entity.GetDefaultTableName().ToSnakeCase());

                // Replace column names            
                foreach (var property in entity.GetProperties())
                {
                    property.SetColumnName(property.GetColumnName().ToSnakeCase());
                }

                foreach (var key in entity.GetKeys())
                {
                    key.SetName(key.GetName().ToSnakeCase());
                }

                foreach (var key in entity.GetForeignKeys())
                {
                    key.SetConstraintName(key.GetDefaultName().ToSnakeCase());
                }

                foreach (var index in entity.GetIndexes())
                {
                    index.SetDatabaseName(index.GetDefaultDatabaseName().ToSnakeCase());
                }
            }
        }

        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
        {

            // 1. We persist the changes first
            _ = await SaveChangesAsync(cancellationToken);

            // If no exceptions, the code should continue, otherwise it exits after exception.

            // Let's grab all the Aggregates that changed state
            var entities = ChangeTracker.Entries();

            foreach (var entityEntry in entities)
            {
                // If it is not an Aggregate, we move to the next EntityEntry
                if (!(entityEntry.Entity is IAggregateRoot)) continue;

                // Let's parse the EntityEntry to is base type AggregateRoot so that we can
                // extract its events and id
                var aggregate = (AggregateRoot)entityEntry.Entity;

                // Let's remove any copy of the Aggregate's DTO from Redis.
                // Since the state of the aggregate changed, the cache is invalid.
                // Wipe it out! :)

                // This needs refactoring... do not use for now.
                //await _cacheValidator.Invalidate(aggregate.Id.ToString());

                // Let's dispatch all the events that accumulated during the Aggregate state changes
                await _eventProcessor.ProcessAsync(aggregate.Events);
            }

            return true;
        }
    }

    internal static class StringExtensions
    {
        internal static string ToSnakeCase(this string input)
        {
            if (string.IsNullOrEmpty(input)) { return input; }

            var startUnderscores = Regex.Match(input, @"^_+");
            return startUnderscores + Regex.Replace(input, @"([a-z0-9])([A-Z])", "$1_$2").ToLower();
        }
    }
}
