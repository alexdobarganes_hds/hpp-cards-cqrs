﻿using Convey;
using HDS.PPC.Cards.Application.Repositories;
using HDS.PPC.Cards.Domain.Aggregates;
using HDS.PPC.Cards.Infrastructure.EntityFramework.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace HDS.PPC.Cards.Infrastructure.EntityFramework
{
    public static class Extensions
    {
        public static IConveyBuilder AddPostgresDb(this IConveyBuilder builder)
        {
            builder.Services.AddScoped<IRepository<Card>, CardsRepository>();
            //builder.Services.AddAutoMapper(new[] { typeof(NotificationTaskMapper).Assembly }, ServiceLifetime.Scoped);
            builder.Services.AddSingleton(builder.GetOptions<PostgresConfig>("postgres"));
            builder.Services.AddDbContext<CardsDbContext>((provider, optionsBuilder) =>
            {
                var config = provider.GetService<PostgresConfig>();
                optionsBuilder
                .UseNpgsql(config.GetConnectionString(), options =>
                {
                    options.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                    options.EnableRetryOnFailure();
                    options.SetPostgresVersion(13, 3);
                });
            });

            return builder;
        }

        public static IApplicationBuilder UsePostgresDb(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>()?.CreateScope())
            {
                var context = serviceScope?.ServiceProvider.GetRequiredService<CardsDbContext>();
                context?.Database.EnsureCreated();
            }

            return app;
        }
    }
}
