﻿using HDS.PPC.Cards.Application.Repositories;
using HDS.PPC.Cards.Domain.Aggregates;
using System;
using System.Threading.Tasks;

namespace HDS.PPC.Cards.Infrastructure.EntityFramework.Repositories
{
    public class CardsRepository : IRepository<Card>
    {
        private readonly CardsDbContext _dbContext;

        public IUnitOfWork UnitOfWork => _dbContext;

        public CardsRepository(CardsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddAsync(Card aggregate)
        {
            await _dbContext.AddAsync(aggregate);
        }

        public Task UpdateAsync(Card aggregate)
        {
            _dbContext.Update(aggregate);
            return Task.CompletedTask;
        }

        public Task DeleteAsync(Card aggregate)
        {
            _dbContext.Remove(aggregate);
            return Task.CompletedTask;
        }

        public async Task<Card> GetAsync(Guid id)
        {
            return await _dbContext.FindAsync<Card>(id);
        }
    }
}