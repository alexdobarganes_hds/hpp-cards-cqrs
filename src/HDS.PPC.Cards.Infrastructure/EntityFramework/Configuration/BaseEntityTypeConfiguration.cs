﻿using HDS.PPC.Cards.Domain.Aggregates;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HDS.PPC.Cards.Infrastructure.EntityFramework.Configuration
{
    public abstract class BaseEntityTypeConfiguration<T> : IEntityTypeConfiguration<T> where T : AggregateRoot
    {
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            builder.Ignore(x => x.Events);
            builder.ToTable(typeof(T).Name);
            builder.HasKey(x => x.Id);
        }
    }
}
