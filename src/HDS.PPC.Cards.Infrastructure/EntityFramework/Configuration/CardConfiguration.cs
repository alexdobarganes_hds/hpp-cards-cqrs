﻿using HDS.PPC.Cards.Domain.Aggregates;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace HDS.PPC.Cards.Infrastructure.EntityFramework.Configuration
{
    public class CardConfiguration : BaseEntityTypeConfiguration<Card>
    {
        public override void Configure(EntityTypeBuilder<Card> builder)
        {
            builder.OwnsMany(x => x.CardActivity);
            builder.OwnsOne(x => x.CardHolder);
            builder.OwnsOne(x => x.CardLimit);
            base.Configure(builder);
        }
    }
}
