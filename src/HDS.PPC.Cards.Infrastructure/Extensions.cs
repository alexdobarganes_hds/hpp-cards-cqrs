using Convey;
using Convey.Docs.Swagger;
using Convey.MessageBrokers;
using Convey.MessageBrokers.CQRS;
using Convey.WebApi;
using Convey.WebApi.Swagger;
using HDS.PPC.Cards.Application.Commands;
using HDS.PPC.Cards.Application.Events;
using HDS.PPC.Cards.Application.Services;
using HDS.PPC.Cards.Infrastructure.Contexts;
using HDS.PPC.Cards.Infrastructure.EntityFramework;
using HDS.PPC.Cards.Infrastructure.Exceptions;
using HDS.PPC.Cards.Infrastructure.Logging;
using HDS.PPC.Cards.Infrastructure.MessageBrokers.PubSub;
using HDS.PPC.Cards.Infrastructure.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HDS.PPC.Cards.Infrastructure
{
    public static class Extensions
    {
        public static IConveyBuilder AddInfrastructure(this IConveyBuilder builder)
        {
            builder.Services.AddHealthChecks();
            builder.Services.AddSingleton<IEventMapper, EventMapper>();
            builder.Services.AddTransient<IEventProcessor, EventProcessor>();

            builder
                .AddHandlersLogging()
                .AddErrorHandler<ExceptionToResponseMapper>()
                .AddExceptionToMessageMapper<ExceptionToMessageMapper>()
                .AddHttpServiceClients()
                .AddWebApiSwaggerDocs()
                .AddPostgresDb()
                .AddPubSub();

            return builder;
        }

        public static IApplicationBuilder UseApp(this IApplicationBuilder app)
        {
            app.UseConvey()
               .UseErrorHandler()
               .UseSwaggerDocs()
               .UsePostgresDb()
               .UsePubSub()
                    .SubscribeCommand<CreateCard>()
                    .SubscribeCommand<CreateCard>()
                    .SubscribeCommand<DisableCard>();
            return app;
        }

        internal static CorrelationContext GetCorrelationContext(this IHttpContextAccessor accessor)
        {
            var json = StringValues.Empty;
            return accessor.HttpContext?.Request.Headers.TryGetValue("Correlation-Context", out json) is true
                           ? JsonConvert.DeserializeObject<CorrelationContext>(json.FirstOrDefault())
                           : null;
        }

        internal static IDictionary<string, object> GetHeadersToForward(this IMessageProperties messageProperties)
        {
            const string sagaHeader = "Saga";
            if (messageProperties?.Headers is null || !messageProperties.Headers.TryGetValue(sagaHeader, out var saga))
            {
                return null;
            }

            return saga is null
                ? null
                : new Dictionary<string, object>
                {
                    [sagaHeader] = saga
                };
        }

        internal static string GetSpanContext(this IMessageProperties messageProperties, string header)
        {
            if (messageProperties is null)
            {
                return string.Empty;
            }

            if (messageProperties.Headers.TryGetValue(header, out var span) && span is byte[] spanBytes)
            {
                return Encoding.UTF8.GetString(spanBytes);
            }

            return string.Empty;
        }
    }
}