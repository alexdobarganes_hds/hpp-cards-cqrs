using HDS.PPC.Cards.Application;

namespace HDS.PPC.Cards.Infrastructure.Contexts
{
    internal interface IAppContextFactory
    {
        IAppContext Create();
    }
}