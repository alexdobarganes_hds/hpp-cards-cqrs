using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Google.Cloud.PubSub.V1;
using Google.Protobuf;
using Grpc.Core;
using HDS.PPC.Cards.Application.Events;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace HDS.PPC.Cards.Infrastructure.MessageBrokers.PubSub.Publishers
{
    internal class PubSubPublisher : IPubSubPublisher
    {
        private readonly PublisherServiceApiClient _publisherServiceApiClient;
        private readonly ILogger<PubSubPublisher> _logger;
        private readonly PubSubConfig _publisherConfig;

        public PubSubPublisher(ILogger<PubSubPublisher> logger, PubSubConfig publisherConfig)
        {
            _logger = logger;
            _publisherConfig = publisherConfig;

            if (_publisherConfig.Emulator != null)
                _publisherServiceApiClient = new PublisherServiceApiClientBuilder
                {
                    Endpoint = _publisherConfig.Emulator.EmulatorHostAndPort,
                    ChannelCredentials = ChannelCredentials.Insecure
                }.Build();
            else
                _publisherServiceApiClient = PublisherServiceApiClient.Create();
        }

        private async Task<string> PublishMessage(TopicName topicName, PubsubMessage pubSubMessage)
        {
            var response = await _publisherServiceApiClient.PublishAsync(topicName, new[] { pubSubMessage });
            return response.MessageIds.Single();
        }

        public async Task PublishAsync<T>(T message, object messageContext = null, IDictionary<string, object> headers = null)
            where T : class
        {
            var topicName = TopicName.FromProjectTopic(_publisherConfig.GcpProjectId, $"{_publisherConfig.LifeCycle}-{GetTopicFromAttribute(message) ?? _publisherConfig.TopicId}");
            try
            {
                var pubSubMessage = BuildPubSubMessage(message, headers);

                var messageId = await PublishMessage(topicName, pubSubMessage).ConfigureAwait(false);

                _logger.LogInformation("Published message with {@messageId} to {@topic}", messageId, topicName.ToString());
            }
            catch (Exception exception)
            {
                _logger.LogInformation("An error occurred when publishing message {@message}: {@exceptionMessage} to {@topic}",
                    message, exception.Message, topicName.ToString());
                throw;
            }
        }

        private PubsubMessage BuildPubSubMessage<T>(T message, IDictionary<string, object> headers)
        {
            var eventType = $"{GetLocalEventTypeFromAttribute(message)}";
            var pubSubInfo = GetPublishInfoFromAttribute(message);

            if (pubSubInfo != null)
                eventType = $"{pubSubInfo.Prefix}.{GetEventTypeFromAttribute(message)}";

            var pubSubMessage = new PubsubMessage
            {
                // The data is any arbitrary ByteString. Here, we're using text.
                Data = ByteString.CopyFromUtf8(JsonConvert.SerializeObject(message))
            };

            pubSubMessage.Attributes.Add("eventType", eventType);
            //pubSubMessage.Attributes.Add("subject", subject);

            //Additional Headers
            if (headers == null) return pubSubMessage;
            foreach (var key in headers.Keys)
            {
                pubSubMessage.Attributes.Add(key, headers[key].ToString());
            }

            return pubSubMessage;
        }

        private static string GetEventTypeFromAttribute<T>(T message) => message.GetType().Name;
        private static string GetLocalEventTypeFromAttribute<T>(T message) => message.GetType().Name;
        private static string GetTopicFromAttribute<T>(T message) => message.GetType().GetCustomAttribute<PubSubInfoAttribute>()?.TopicId;
        private static PubSubInfoAttribute GetPublishInfoFromAttribute<T>(T message) => message.GetType().GetCustomAttribute<PubSubInfoAttribute>();
    }
}