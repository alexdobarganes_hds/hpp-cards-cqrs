﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace HDS.PPC.Cards.Infrastructure.MessageBrokers.PubSub.Publishers
{
    internal interface IPubSubPublisher
    {
        Task PublishAsync<T>(T message, object correlationContext = null, IDictionary<string, object> headers = null) where T : class;
    }
}
