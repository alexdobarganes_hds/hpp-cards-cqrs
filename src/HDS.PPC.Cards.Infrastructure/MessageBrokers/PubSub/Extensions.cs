using Convey;
using Convey.CQRS.Commands;
using Convey.CQRS.Events;
using Convey.MessageBrokers;
using HDS.PPC.Cards.Infrastructure.Exceptions;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System.Runtime.CompilerServices;
using HDS.PPC.Cards.Infrastructure.MessageBrokers.PubSub.Publishers;
using HDS.PPC.Cards.Infrastructure.MessageBrokers.PubSub.Subscribers;

[assembly: InternalsVisibleTo("HDS.PPC.Cards.Infrastructure.Integration.Tests")]
namespace HDS.PPC.Cards.Infrastructure.MessageBrokers.PubSub
{
    internal static class Extensions
    {
        private const string SectionName = "pubsub";

        internal static IConveyBuilder AddExceptionToMessageMapper<T>(this IConveyBuilder builder)
            where T : class, IExceptionToMessageMapper
        {
            builder.Services.AddSingleton<IExceptionToMessageMapper, T>();
            return builder;
        }

        internal static string GetMessageName(this object message)
            => message.GetType().Name.Underscore().ToLowerInvariant();

        internal static IConveyBuilder AddPubSub(this IConveyBuilder builder, string sectionName = SectionName)
        {
            if (string.IsNullOrWhiteSpace(sectionName))
            {
                sectionName = SectionName;
            }

            var options = builder.GetOptions<PubSubConfig>(sectionName);

            builder.Services.AddSingleton(options);
            builder.Services.AddSingleton<IPubSubPublisher, PubSubPublisher>();
            builder.Services.AddTransient<ICorrelationContextAccessor, CorrelationContextAccessor>();

            builder.AddPubSubCommandDispatcher()
                   .AddPubSubEventDispatcher();

            return builder;
        }

        internal static IConveyBuilder AddPubSubCommandDispatcher(this IConveyBuilder builder)
        {
            builder.Services.AddTransient<ICommandDispatcher, PubSubDispatcher>();
            return builder;
        }

        internal static IConveyBuilder AddPubSubEventDispatcher(this IConveyBuilder builder)
        {
            builder.Services.AddTransient<IEventDispatcher, PubSubDispatcher>();
            return builder;
        }

        internal static IBusSubscriber UsePubSub(this IApplicationBuilder app)
        {
            return new PubSubSubscriber(app);
        }
    }
}