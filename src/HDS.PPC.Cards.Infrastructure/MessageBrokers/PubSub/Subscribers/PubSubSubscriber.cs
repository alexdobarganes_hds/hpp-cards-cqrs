﻿using System;
using System.IO;
using System.Threading.Tasks;
using Convey.CQRS.Events;
using Convey.MessageBrokers;
using Google.Cloud.PubSub.V1;
using HDS.PPC.Cards.Infrastructure.Exceptions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HDS.PPC.Cards.Infrastructure.MessageBrokers.PubSub.Subscribers
{
    public class PubSubSubscriber : IBusSubscriber
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IApplicationBuilder _app;
        private readonly PubSubConfig _pubSubConfig;
        private readonly ILogger<PubSubSubscriber> _logger;
        private readonly IExceptionToMessageMapper _exceptionToMessageMapper;
        private readonly IEventDispatcher _eventDispatcher;
        private bool _disposedValue;

        public PubSubSubscriber(IApplicationBuilder app)
        {
            _app = app;
            _serviceProvider = app.ApplicationServices;
            _pubSubConfig = _serviceProvider.GetRequiredService<PubSubConfig>();
            _logger = _serviceProvider.GetRequiredService<ILogger<PubSubSubscriber>>();
            _exceptionToMessageMapper = _serviceProvider.GetService<IExceptionToMessageMapper>() ?? new EmptyExceptionToMessageMapper();
            _eventDispatcher = _serviceProvider.GetRequiredService<IEventDispatcher>();
        }

        private Task TryHandle<TEvent>(TEvent @event, Func<IServiceProvider, TEvent, object, Task> handle)
        {
            return handle(_serviceProvider, @event, null);
        }

        private static async Task<PubsubMessage> GetPubSubMessage(HttpRequest request)
        {
            request.EnableBuffering();
            dynamic bodyContent;
            using (var reader = new StreamReader(request.Body, System.Text.Encoding.UTF8, false, leaveOpen: true))
            {
                bodyContent = await reader.ReadToEndAsync();
                request.Body.Position = 0;
            }

            var message = JObject.Parse(bodyContent).message;

            return PubsubMessage.Parser.ParseJson(JsonConvert.SerializeObject(message));
        }

        public IBusSubscriber Subscribe<T>(Func<IServiceProvider, T, object, Task> handle) where T : class
        {
            var typeName = typeof(T).Name;

            _logger.Log(LogLevel.Information, "PubSubSubscriber successfully registered {@eventType}", typeName);

            _app.Use(async (context, next) =>
            {
                if (context.Request.Path.Value == null || !context.Request.Path.Value.Contains(_pubSubConfig.Endpoint))
                    await next.Invoke();
                else
                {
                    var pubSubMessage = await GetPubSubMessage(context.Request);

                    if (!IsEventRegistered(pubSubMessage, typeName))
                        context.Response.StatusCode = StatusCodes.Status200OK;
                    else
                        await HandleMessage(handle, context, pubSubMessage);
                }
            });

            return this;
        }

        private static bool IsEventRegistered(PubsubMessage pubSubMessage, string typeName)
        {
            return pubSubMessage.Attributes["eventType"] == typeName;
        }

        private async Task HandleMessage<T>(Func<IServiceProvider, T, object, Task> handle, HttpContext context, PubsubMessage pubSubMessage) where T : class
        {
            var payload = pubSubMessage.Data.ToStringUtf8();
            var message = JsonConvert.DeserializeObject<T>(payload);
            var messageName = message.GetMessageName();

            _logger.LogInformation("Received PubSub HTTP Request, {@messageName} with content {@message}", message, messageName);

            try
            {
                _logger.LogInformation("Handling {@messageName} with content {@message}.", messageName, message);
                await TryHandle(message, handle);
                _logger.LogInformation("Handled {@messageName} with content {@message}.", messageName, message);
            }
            catch (Exception ex)
            {
                if (_exceptionToMessageMapper.Map(ex, message) is not IRejectedEvent rejectedEvent)
                    throw new Exception($"Unable to handle a message: '{messageName}' ", ex);

                await _eventDispatcher.PublishAsync(rejectedEvent);
                _logger.LogWarning("Published a rejected event: {@rejectedEvent} for the message: {@messageName}.", rejectedEvent.GetMessageName(), messageName);
            }

            _logger.LogInformation("Acknowledged PubSub HTTP Request, {@messageName} with content {@message}", message, messageName);

            context.Response.StatusCode = StatusCodes.Status200OK;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        private class EmptyExceptionToMessageMapper : IExceptionToMessageMapper
        {
            public object Map(Exception exception, object message) => null;
        }
    }
}