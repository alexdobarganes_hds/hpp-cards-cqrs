﻿using Convey.CQRS.Commands;
using Convey.CQRS.Events;
using Convey.MessageBrokers;
using HDS.PPC.Cards.Infrastructure.MessageBrokers.PubSub.Publishers;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace HDS.PPC.Cards.Infrastructure.MessageBrokers.PubSub
{
    internal sealed class PubSubDispatcher : ICommandDispatcher, IEventDispatcher
    {
        private readonly ILogger<PubSubDispatcher> _logger;
        private readonly IPubSubPublisher _pubSubPublisher;
        private readonly ICorrelationContextAccessor _accessor;

        public PubSubDispatcher(ILogger<PubSubDispatcher> logger, IPubSubPublisher pubSubPublisher, ICorrelationContextAccessor accessor)
        {
            _logger = logger;
            _pubSubPublisher = pubSubPublisher;
            _accessor = accessor;
        }

        public Task PublishAsync<T>(T @event) where T : class, IEvent

        {
            _logger.LogInformation("Dispatching event {@event} to PubSub.", @event);
            return _pubSubPublisher.PublishAsync(@event, _accessor.CorrelationContext);
        }

        public Task SendAsync<T>(T command) where T : class, ICommand
        {
            _logger.LogInformation("Dispatching command {@command} to PubSub.", command);
            return _pubSubPublisher.PublishAsync(command, _accessor.CorrelationContext);
        }
    }
}