using HDS.PPC.Cards.Application;

namespace HDS.PPC.Cards.Infrastructure
{
    public interface IAppContextFactory
    {
        IAppContext Create();
    }
}