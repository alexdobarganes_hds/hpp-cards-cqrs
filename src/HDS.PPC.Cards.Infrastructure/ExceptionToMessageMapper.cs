using HDS.PPC.Cards.Application.Commands;
using HDS.PPC.Cards.Application.Events.Rejected;
using HDS.PPC.Cards.Application.Exceptions;
using System;

namespace HDS.PPC.Cards.Infrastructure.Exceptions
{
    public class ExceptionToMessageMapper : IExceptionToMessageMapper
    {
        public object Map(Exception exception, object message)
            => exception switch
            {
                CardAlreadyCreatedException ex => message switch
                {
                    CreateCard command => new CreateCardRejected(command.CardId, ex.Message, ex.Code),
                    _ => null,
                },
                _ => null
            };
    }
}