using Convey;
using Convey.Logging.CQRS;
using HDS.PPC.Cards.Application.Commands;
using Microsoft.Extensions.DependencyInjection;

namespace HDS.PPC.Cards.Infrastructure.Logging
{
    internal static class Extensions
    {
        public static IConveyBuilder AddHandlersLogging(this IConveyBuilder builder)
        {
            var assembly = typeof(CreateCard).Assembly;

            builder.Services.AddSingleton<IMessageToLogTemplateMapper>(new MessageToLogTemplateMapper());

            return builder
                .AddCommandHandlersLogging(assembly)
                .AddEventHandlersLogging(assembly);
        }
    }
}