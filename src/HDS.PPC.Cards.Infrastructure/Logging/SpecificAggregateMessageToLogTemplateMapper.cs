using Convey.Logging.CQRS;
using HDS.PPC.Cards.Application.Commands;
using HDS.PPC.Cards.Application.Exceptions;
using System;
using System.Collections.Generic;

namespace HDS.PPC.Cards.Infrastructure.Logging
{
    internal sealed class MessageToLogTemplateMapper : IMessageToLogTemplateMapper
    {
        private static IReadOnlyDictionary<Type, HandlerLogTemplate> MessageTemplates
            => new Dictionary<Type, HandlerLogTemplate>
            {
                 {
                     typeof(CreateCard),
                     new HandlerLogTemplate
                     {
                         After = "Created a Card with id: {CardId}. ",
                         OnError = new Dictionary<Type, string>
                         {
                             { typeof(CardAlreadyCreatedException), "Resource with id: {CardId} already created."}
                         }
                     }
                 }
            };

        public HandlerLogTemplate Map<TMessage>(TMessage message) where TMessage : class
        {
            var key = message.GetType();
            return MessageTemplates.TryGetValue(key, out var template) ? template : null;
        }
    }
}